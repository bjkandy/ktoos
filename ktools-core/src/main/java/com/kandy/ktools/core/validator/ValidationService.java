package com.kandy.ktools.core.validator;


import com.kandy.ktools.core.exception.ServiceException;

public interface ValidationService {

    ValidationResult validate(Object param, boolean fastMode);

    void validate(Object param) throws ServiceException;

}
